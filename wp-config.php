<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '})csbIBr3DGmY$1$&LmetT)&y{<P!`=3Ps%zR/kem}N8 DIlMJRx>B,8sQ.qD!k3' );
define( 'SECURE_AUTH_KEY',  'f,@?>cs,rYFr[5=Tb;Ud.Hp/to/W!.`:^U0:D`tyVSsw:H[-<yfF; jL6~:ONo4M' );
define( 'LOGGED_IN_KEY',    '?0@B2pPFb]{1>;b`@3;JMa[#.ts:>GP~{&Z[))}s;6bIT=@ubZTmXM*%P^tbY*FS' );
define( 'NONCE_KEY',        '&H%GF|&iOy$#W0eRMO2b*A%4~xf66N`d+#P[VklSs[N-B@5&Y{H4C*k1?.K5qX]!' );
define( 'AUTH_SALT',        'Z9#Yog=pIv>2=qIO0z5gn{? bBI74qHXsw^11T3sbWl4z_lavW[RnBY}YR08b^qM' );
define( 'SECURE_AUTH_SALT', 'VLyXq0$rObCDLBxKx)}7/sYP$eheW bAwH6Sw([o+O?O%T%=FFh0vb7+SO363<:A' );
define( 'LOGGED_IN_SALT',   '@Yn6&:Ck:{jz}g2tiTzB=,,PgS&dhC_k 6Ayz_P2j<t%u|1QKa`~J#p<7WPfLbQ9' );
define( 'NONCE_SALT',       'iFk+9a{v<,bHC_%(gK5y,v^J @cE^?Gi8^t{g<r(l]NwWMlPi4K.&q4Kcl~N9B|:' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
